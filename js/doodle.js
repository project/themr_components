var excalidrawAPI, setExcalidrawAPI, canvasUrl, setCanvasUrl;
const App = () => {

  [excalidrawAPI, setExcalidrawAPI] = React.useState(null);

  return React.createElement(
    React.Fragment,
    null,
    React.createElement(
      "button",
      {
        id: "themr-rerender",
        className: "custom-button",
        onClick: async (e) => {
          e.preventDefault();
          if (!excalidrawAPI) {
            return
          }
          const elements = excalidrawAPI.getSceneElements();
          if (!elements || !elements.length) {
            return
          }
          const blob = await ExcalidrawLib.exportToBlob({
            elements,
            appState: {
              viewBackgroundColor: "#ffffff",
              exportWithDarkMode: false,
            },
            files: excalidrawAPI.getFiles(),
            getDimensions: (width, height) => { return { width: width, height: height } }
          });
          let reader = new FileReader();
          reader.readAsDataURL(blob);
          reader.onloadend = function () {
            let base64data = reader.result;
            document.getElementById('doodle-base-64').value = base64data;
            document.getElementById('edit-submit').click();
          }
        }
      },
      "Export",
    ),
    React.createElement(
      "div",
      {
        style: { height: "750px" },
      },
      React.createElement(ExcalidrawLib.Excalidraw, { ref: (api) => setExcalidrawAPI(api), initialData: {appState: { viewBackgroundColor: "#eeeeee" }}}),
    ),
  );
};
const excalidrawWrapper = document.getElementById("doodle");
const root = ReactDOM.createRoot(excalidrawWrapper);
root.render(React.createElement(App));

(function ($) {
  $(document).ready(function() {
    $('#edit-fake-submit').on('click', function (event) {
      event.preventDefault();
      $('#themr-rerender').click();
      return false;
    })
  })
})(jQuery);
