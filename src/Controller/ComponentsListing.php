<?php

namespace Drupal\themr_components\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Lists the components for modification.
 */
class ComponentsListing extends ControllerBase {

  /**
   * Lists all components.
   */
  public function list() {
    $response = [
      '#markup' => 'hello',
    ];
    return $response;
  }
}
