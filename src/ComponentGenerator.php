<?php

namespace Drupal\themr_components;

use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\themr_components\PluginManager\ThemrFieldManager;

/**
 * Component Generator.
 */
class ComponentGenerator {

  /**
   * The entitytype manager.
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The generators.
   */
  protected array $generators = [];

  /**
   * Mapping of children.
   */
  protected array $children = [];

  /**
   * Constructs a new component.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity manager.
   * @param \Drupal\themr_components\PluginManager\ThemrFieldManager $themrFieldManager
   *   The Themr field manager.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, ThemrFieldManager $themrFieldManager) {
    $this->entityTypeManager = $entityTypeManager;
    foreach ($themrFieldManager->getDefinitions() as $definition) {
      $this->generators[$definition['id']] = $themrFieldManager->createInstance($definition['id']);
    }
  }

  /**
   * Generate a component.
   *
   * @param string $name
   *   The human name.
   * @param array $config
   *   The model to use.
   * @param string $entityType
   *   The base entity type.
   *
   * @return bool
   *   If it worked.
   */
  public function generateComponent($name, array $config, $entityType = 'paragraphs'): bool {
    $dataName = $this->getDataName($name);
    if ($this->generateEntityBundle($name, $dataName)) {
      $this->generateChildEntities($config);
      $this->generateFields($dataName, $config);
      $this->generateDisplay($dataName, $config);
      $this->generateFormDisplay($dataName, $config);
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Generate a new paragraphs bundle.
   *
   * @param string $name
   *   The human readable name.
   *
   * @return bool
   *   If successful.
   */
  protected function generateEntityBundle($name, $dataName): bool {
    $paragraphsType = $this->entityTypeManager->getStorage('paragraphs_type');
    if (!$paragraphsType->load($dataName)) {
      $newType = $paragraphsType->create([
        'id' => $dataName,
        'label' => $name,
        'revision' => TRUE,
      ]);
      return $newType->save();
    }
    return TRUE;
  }

  /**
   * Generate possible child entities.
   *
   * @return bool
   *   If successful.
   */
  protected function generateChildEntities($config): bool {
    $children = [];
    $this->findChildEntities($config, $children);
    foreach ($children as $child) {
      if ($this->generateComponent($child['field-name'], $child['contains'])) {
        $this->children[] = $this->getDataName($child['field-name']);
      }
    }
    return TRUE;
  }

  /**
   * Generate possible child entities.
   *
   * @return bool
   *   If successful.
   */
  protected function generateFields($dataName, $config): bool {
    $fields = [];
    $this->findFields($config, $fields);
    $weight = 0;
    foreach ($fields as $field) {
      $this->generators[$field['field-type']]->generate($dataName, $field, $weight);
      $weight++;
    }
    return TRUE;
  }

  /**
   * Generate the displays.
   *
   * @param string $dataName
   *   The data name of the paragraph.
   * @param array $config
   *   The configuration.
   * @param string $parent
   *   The parent name
   */
  protected function generateDisplay($dataName, array $config, $parent = '') {
    $display = EntityViewDisplay::load("paragraph.$dataName.default") ?:
    EntityViewDisplay::create([
      'targetEntityType' => 'paragraph',
      'bundle' => $dataName,
      'mode' => 'default',
      'status' => TRUE,
    ])->save();
    $weight = 0;
    foreach ($config as $part) {
      if (!empty($part['field-type'])) {
        $this->generators[$part['field-type']]->display($dataName, $part, $parent, $weight);
      }
      else if (!empty($part['contains'])) {
        $display = EntityViewDisplay::load("paragraph.$dataName.default");
        $name = $part['container-name'] ?? $part['type'];
        $id = $this->getDataName($name);
        $children = [];
        foreach ($part['contains'] as $child) {
          $childId = $child['container-name'] ?? '';
          $childId = !$childId && !empty($child['field-name']) ? $child['field-name'] : $childId;
          $childId = $childId ? $childId : $child['type'];
          $children[] = $this->getDataName($childId);
        }
        $display->setThirdPartySetting('field_group', $id, [
          'label' => $name,
          'parent_name' => $parent,
          'region' => 'content',
          'weight' => $weight,
          'children' => $children,
          'format_type' => 'html_element',
          'format_settings' => [
            'classes' => $part['attributes']['class'] ?? '',
            'show_empty_fields' => TRUE,
            'element' => $part['type'],
            'show_label' => FALSE,
          ],
        ])->save();
        $this->generateDisplay($dataName, $part['contains'], $id);
      }
      $weight++;
    }

  }

  /**
   * Generate the forms.
   *
   * @param string $dataName
   *   The data name of the paragraph.
   * @param array $config
   *   The configuration.
   * @param string $parent
   *   The parent name
   */
  protected function generateFormDisplay($dataName, $config, $parent = "") {
    $weight = 0;
    foreach ($config as $part) {
      if (!empty($part['field-type'])) {
        $this->generators[$part['field-type']]->formDisplay($dataName, $part, $parent, $weight);
      }
      else if (!empty($part['contains'])) {
        $name = $part['container-name'] ?? $part['type'];
        $id = $this->getDataName($name);
        $this->generateFormDisplay($dataName, $part['contains'], $id);
      }
      $weight++;
    }
  }

  /**
   * Find all field.
   */
  protected function findFields($config, &$fields) {
    foreach ($config as $part) {
      if (!empty($part['field-type'])) {
        $fields[] = $part;
      }
      else if (!empty($part['contains'])) {
        $this->findFields($part['contains'], $fields);
      }
    }
  }

  /**
   * Find all children.
   */
  protected function findChildEntities($config, &$children) {
    foreach ($config as $part) {
      if (!empty($part['field-type']) && $part['field-type'] == 'custom_field') {
        $children[] = $part;
      }
      else if (!empty($part['contains'])) {
        $this->findChildEntities($part['contains'], $children);
      }
    }
  }

  /**
   * Generate a data name.
   *
   * @param string $name
   *   Human readable name to generate from.
   *
   * @return string
   *   The data name.
   */
  protected function getDataName($name) {
    return substr(strtolower(str_replace(' ', '_', $name)), 0, 32);
  }
}
