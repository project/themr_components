<?php

namespace Drupal\themr_components\PluginInterfaces;

/**
 * Interface for Themr Fields.
 */
interface ThemrFieldInterface {

  /**
   * Generate a field.
   *
   * @param string $dataName
   *   The data name of the paragraphs.
   * @param array $config
   *   The field configuration.
   * @param int $weight
   *   The weight of creation.
   *
   * @return bool
   *   Success or not.
   */
  public function generate($dataName, array $config, $weight = 0): bool;

  /**
   * Generate a field display.
   *
   * @param string $dataName
   *   The data name of the paragraphs.
   * @param array $config
   *   The field configuration.
   * @param string $parent
   *   The parent name.
   * @param int $weight
   *   The weight of creation.
   *
   * @return bool
   *   Success or not.
   */
  public function display($dataName, array $config, $parent, $weight = 0): bool;

  /**
   * Generate a form display.
   *
   * @param string $dataName
   *   The data name of the paragraphs.
   * @param array $config
   *   The field configuration.
   * @param string $parent
   *   The parent name.
   * @param int $weight
   *   The weight of creation.
   *
   * @return bool
   *   Success or not.
   */
  public function formDisplay($dataName, array $config, $parent, $weight = 0): bool;

}
