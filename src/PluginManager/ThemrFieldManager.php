<?php

namespace Drupal\themr_components\PluginManager;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Provides an Themr Field rule plugin manager.
 *
 * @see \Drupal\themr_components\Annotation\ThemrField
 * @see \Drupal\themr_components\PluginInterfaces\ThemrFieldInterface
 * @see plugin_api
 */
class ThemrFieldManager extends DefaultPluginManager {

  /**
   * Constructs a ThemrFieldManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/ThemrField',
      $namespaces,
      $module_handler,
      'Drupal\themr_components\PluginInterfaces\ThemrFieldInterface',
      'Drupal\themr_components\Annotation\ThemrField'
    );
    $this->alterInfo('themr_components_field');
    $this->setCacheBackend($cache_backend, 'themr_components_field_plugins');
  }

}
