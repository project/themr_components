<?php

namespace Drupal\themr_components\Annotation;

use Drupal\Component\Annotation\Plugin;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Themr field generator.
 *
 * With helper functions.
 *
 * @ingroup themr_components_field
 *
 * @Annotation
 */
class ThemrField extends Plugin {

  // All should be translatable.
  use StringTranslationTrait;

  /**
   * The plugin ID.
   */
  public string $id;

  /**
   * The description for OpenAI.
   */
  public string $description;

  /**
   * Generate a data name.
   *
   * @param string $name
   *   Human readable name to generate from.
   *
   * @return string
   *   The data name.
   */
  protected function getDataName($name) {
    return substr(strtolower(str_replace(' ', '_', $name)), 0, 32);
  }

}
