<?php

namespace Drupal\themr_components\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\entity_reference_revisions\Plugin\Field\FieldFormatter\EntityReferenceRevisionsEntityFormatter;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Themr Paragraphs field formatter.
 *
 * @FieldFormatter(
 *   id = "themr_paragraphs",
 *   label = @Translation("Themr Paragraphs"),
 *   description = @Translation("Renders paragraphs with Bootstrap body."),
 *   field_types = {
 *     "entity_reference_revisions"
 *   }
 * )
 */
class ThemrParagraphFormatter extends EntityReferenceRevisionsEntityFormatter implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = parent::viewElements($items, $langcode);
    $elements['#attributes']['class'][] = 'marcus';
    $elements['#item_attributes']['class'][] = 'marcus';
    $entities = $this->getEntitiesToView($items, $langcode);
    foreach ($entities as $delta => $entity) {
      // Add class.
      $elements[$delta]['#attributes']['class'][] = 'col';
    }

    return $elements;
  }

}
