<?php

namespace Drupal\themr_components\Plugin\ThemrField;

use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\image\Entity\ImageStyle;
use Drupal\themr_components\Annotation\ThemrField;
use Drupal\themr_components\PluginInterfaces\ThemrFieldInterface;

/**
 * The rules for a image field.
 *
 * @ThemrField(
 *   id = "image",
 *   description = "One or more images"
 * )
 */
class ThemrImage extends ThemrField implements ThemrFieldInterface {

  /**
   * {@inheritDoc}
   */
  public function generate($dataName, array $config, $weight = 0): bool {

    // Generate image style.
    $this->generateImageStyles($config);
    $id = $this->getDataName($config['field-name']);
    // Storage generation.
    if (!FieldStorageConfig::load("paragraph.$id")) {
      FieldStorageConfig::create([
        'field_name' => $id,
        'entity_type' => 'paragraph',
        'type' => 'image',
        'settings' => [
          'file_extensions' => 'png jpg jpeg',
          'alt_field' => TRUE,
          'alt_field_required' => FALSE,
          'title_field' => FALSE,
          'title_field_required' => FALSE,
        ],
        'cardinality' => $config['field-item-amount'],
      ])->save();
    }

    // Config generation.
    if (!FieldConfig::load("paragraph.$dataName.$id")) {
      FieldConfig::create([
        'field_name' => $id,
        'entity_type' => 'paragraph',
        'bundle' => $dataName,
        'label' => $config['field-name'],
        'settings' => [
          'file_directory' => 'images/[date:custom:Y]-[date:custom:m]',
          'file_extensions' => 'png jpg jpeg',
          'alt_field_required' => FALSE,
          'min_resolution' => $config['max-image-width'] . 'x' . $config['max-image-height'],
        ],
        'required' => FALSE,
      ])->save();
    }
    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  public function display($dataName, array $config, $parent, $weight = 0): bool {
    $id = $this->getDataName($config['field-name']);
    $display = EntityViewDisplay::load("paragraph.$dataName.default") ?:
        EntityViewDisplay::create([
         'targetEntityType' => 'paragraph',
          'bundle' => $dataName,
          'mode' => 'default',
          'status' => TRUE,
        ]);

    if ($display) {
      $display->setComponent($id, [
        'type' => 'image_class',
        'label' => 'hidden',
        'weight' => $weight,
        'region' => 'content',
        'settings' => [
          'image_style' => $id,
          'image_load' => '',
          'image_loading' => [
            'attribute' => 'lazy',
          ],
          'class' => $config['attributes']['class'] ?? '',
        ],
      ])->save();
    }
    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  public function formDisplay($dataName, array $config, $parent, $weight = 0): bool {
    $id = $this->getDataName($config['field-name']);
    $form = EntityFormDisplay::load("paragraph.$dataName.default") ?:
        EntityFormDisplay::create([
         'targetEntityType' => 'paragraph',
          'bundle' => $dataName,
          'mode' => 'default',
          'status' => TRUE,
        ]);
    if ($form) {
      $form->setComponent($id, [
        'type' => 'image_focal_point',
        'settings' => [
          'progress_indicator' => 'throbber',
          'preview_image_style' => 'thumbnail',
          'preview_link' => TRUE,
          'offset' => '50,50'
        ],
        'weight' => $weight,
      ])->save();
    }
    return TRUE;
  }

  /**
   * Generate image styles.
   */
  protected function generateImageStyles($config) {
    $id = $this->getDataName($config['field-name']);
    if (!ImageStyle::load($id)) {
      $style = ImageStyle::create([
        'name' => $id,
        'label' => $config['field-name'],
      ]);

      $style->addImageEffect([
        'id' => 'focal_point_scale_and_crop',
        'data' => [
          'width' => $config['max-image-width'],
          'height' => $config['max-image-height'],
        ],
      ]);

      $style->save();
    }
  }

}
