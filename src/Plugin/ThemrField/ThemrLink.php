<?php

namespace Drupal\themr_components\Plugin\ThemrField;

use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\themr_components\Annotation\ThemrField;
use Drupal\themr_components\PluginInterfaces\ThemrFieldInterface;

/**
 * The rules for a link field.
 *
 * @ThemrField(
 *   id = "link",
 *   description = "One or more text links or text link buttons"
 * )
 */
class ThemrLink extends ThemrField implements ThemrFieldInterface {

  /**
   * {@inheritDoc}
   */
  public function generate($dataName, array $config, $weight = 0): bool {
    $id = $this->getDataName($config['field-name']);
    // Storage generation.
    if (!FieldStorageConfig::load("paragraph.$id")) {
      FieldStorageConfig::create([
        'field_name' => $id,
        'entity_type' => 'paragraph',
        'type' => 'link',
        'cardinality' => $config['field-item-amount'],
      ])->save();
    }

    // Config generation.
    if (!FieldConfig::load("paragraph.$dataName.$id")) {
      FieldConfig::create([
        'field_name' => $id,
        'entity_type' => 'paragraph',
        'bundle' => $dataName,
        'settings' => [
          'title' => 2,
          'link_type' => 17,
        ],
        'label' => $config['field-name'],
        'required' => FALSE,
      ])->save();
    }

    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  public function display($dataName, array $config, $parent, $weight = 0): bool {
    $id = $this->getDataName($config['field-name'] ?? $config['field-type']);
    $display = EntityViewDisplay::load("paragraph.$dataName.default") ?:
        EntityViewDisplay::create([
         'targetEntityType' => 'paragraph',
          'bundle' => $dataName,
          'mode' => 'default',
          'status' => TRUE,
        ]);

    if ($display) {
      $display->setComponent($id, [
        'type' => 'link_class',
        'label' => 'hidden',
        'settings' => [
          'trim_length' => 80,
          'url_only' => FALSE,
          'url_plain' => FALSE,
          'rel' => '',
          'target' => '',
          'class' => $config['attributes']['class'] ?? '',
        ],
        'weight' => $weight,
        'region' => 'content',
      ])->save();
    }
    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  public function formDisplay($dataName, array $config, $parent, $weight = 0): bool {
    $id = $this->getDataName($config['field-name']);
    $form = EntityFormDisplay::load("paragraph.$dataName.default") ?:
        EntityFormDisplay::create([
         'targetEntityType' => 'paragraph',
          'bundle' => $dataName,
          'mode' => 'default',
          'status' => TRUE,
        ]);
    if ($form) {
      $form->setComponent($id, [
        'type' => 'link_default',
        'settings' => [
          'placeholder_url' => '',
          'placeholder_title' => '',
        ],
        'weight' => $weight,
      ])->save();
    }
    return TRUE;
  }

}
