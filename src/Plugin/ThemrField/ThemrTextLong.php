<?php

namespace Drupal\themr_components\Plugin\ThemrField;

use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\themr_components\Annotation\ThemrField;
use Drupal\themr_components\PluginInterfaces\ThemrFieldInterface;

/**
 * The rules for a text long field.
 *
 * @ThemrField(
 *   id = "text_long",
 *   description = "A long text field with HTML markup"
 * )
 */
class ThemrTextLong extends ThemrField implements ThemrFieldInterface {

  /**
   * {@inheritDoc}
   */
  public function generate($dataName, array $config, $weight = 0): bool {
    $id = $this->getDataName($config['field-name']);
    // Storage generation.
    if (!FieldStorageConfig::load("paragraph.$id")) {
      FieldStorageConfig::create([
        'field_name' => $id,
        'entity_type' => 'paragraph',
        'type' => 'text_long',
        'cardinality' => $config['field-item-amount'],
      ])->save();
    }

    // Config generation.
    if (!FieldConfig::load("paragraph.$dataName.$id")) {
      FieldConfig::create([
        'field_name' => $id,
        'entity_type' => 'paragraph',
        'bundle' => $dataName,
        'label' => $config['field-name'],
        'required' => FALSE,
      ])->save();
    }

    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  public function display($dataName, array $config, $parent, $weight = 0): bool {
    $id = $this->getDataName($config['field-name']);
    $display = EntityViewDisplay::load("paragraph.$dataName.default") ?:
        EntityViewDisplay::create([
         'targetEntityType' => 'paragraph',
          'bundle' => $dataName,
          'mode' => 'default',
          'status' => TRUE,
        ]);

    if ($display) {
      $display->setComponent($id, [
        'type' => 'wrapper_class',
        'label' => 'hidden',
        'settings' => [
          'tag' => $config['type'],
          'summary' => FALSE,
          'class' => $config['attributes']['class'] ?? '',
        ],
        'region' => 'content',
        'weight' => $weight,
      ])->save();
    }
    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  public function formDisplay($dataName, array $config, $parent, $weight = 0): bool {
    $id = $this->getDataName($config['field-name']);
    $form = EntityFormDisplay::load("paragraph.$dataName.default") ?:
        EntityFormDisplay::create([
         'targetEntityType' => 'paragraph',
          'bundle' => $dataName,
          'mode' => 'default',
          'status' => TRUE,
        ]);
    if ($form) {
      $form->setComponent($id, [
        'type' => 'text_textarea',
        'settings' => [
          'rows' => 5,
          'placeholder' => $config['field-placeholder'] ?? '',
        ],
        'weight' => $weight,
      ])->save();
    }
    return TRUE;
  }

}
