<?php

namespace Drupal\themr_components\Plugin\ThemrField;

use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\image\Entity\ImageStyle;
use Drupal\themr_components\Annotation\ThemrField;
use Drupal\themr_components\PluginInterfaces\ThemrFieldInterface;

/**
 * The rules for a reference field.
 *
 * @ThemrField(
 *   id = "custom_field",
 *   description = "One or more referenced entities"
 * )
 */
class ThemrReference extends ThemrField implements ThemrFieldInterface {

  /**
   * {@inheritDoc}
   */
  public function generate($dataName, array $config, $weight = 0): bool {
    $id = $this->getDataName($config['field-name']);

    // Storage generation.
    if (!FieldStorageConfig::load("paragraph.$id")) {
      FieldStorageConfig::create([
        'field_name' => $id,
        'entity_type' => 'paragraph',
        'type' => 'entity_reference_revisions',
        'settings' => [
          'target_type' => 'paragraph',
          'handler' => 'default:paragraph',
        ],
        'cardinality' => $config['field-item-amount'],
      ])->save();
    }

    // Config generation.
    if (!FieldConfig::load("paragraph.$dataName.$id")) {
      FieldConfig::create([
        'field_name' => $id,
        'entity_type' => 'paragraph',
        'bundle' => $dataName,
        'label' => $config['field-name'],
        'settings' => [
          'handler' => 'default:paragraph',
          'handler_settings' => [
            'target_bundles' => [
              $id => $id,
            ],
          ],
        ],
        'required' => FALSE,
      ])->save();
    }
    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  public function display($dataName, array $config, $parent, $weight = 0): bool {
    $id = $this->getDataName($config['field-name']);
    $display = EntityViewDisplay::load("paragraph.$dataName.default") ?:
        EntityViewDisplay::create([
         'targetEntityType' => 'paragraph',
          'bundle' => $dataName,
          'mode' => 'default',
          'status' => TRUE,
        ]);

    if ($display) {
      $display->setComponent($id, [
        'type' => 'entity_reference_revisions_entity_view',
        'label' => 'hidden',
        'settings' => [
          'view_mode' => 'default',
        ],
        'weight' => $weight,
        'region' => 'content',
        'third_party_settings' => [
          'field_formatter_class' => [
            'class' => $config['attributes']['class'] ?? '',
          ],
        ],
      ])->save();
    }
    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  public function formDisplay($dataName, array $config, $parent, $weight = 0): bool {
    $id = $this->getDataName($config['field-name']);
    $form = EntityFormDisplay::load("paragraph.$dataName.default") ?:
        EntityFormDisplay::create([
         'targetEntityType' => 'paragraph',
          'bundle' => $dataName,
          'mode' => 'default',
          'status' => TRUE,
        ]);
    if ($form) {
      $form->setComponent($id, [
        'type' => 'paragraphs',
        'settings' => [
          'title' => $config['field-name'] ?? 'Paragraph',
          'title_plural' => !empty($config['field-name']) ? $config['field-name'] . 's' : 'Paragraphs',
          'edit_mode' => 'closed',
          'autocollapse' => 'none',
          'closed_mode_threshold' => 3,
          'add_mode' => 'dropdown',
          'form_display_mode' => 'default',
          'default_paragraph_type' => '',
          'features' => [
            'collapse_edit_all' => 'collapse_edit_all',
            'duplicate' => 'duplicate',
          ]
        ],
        'weight' => $weight,
      ])->save();
    }
    return TRUE;
  }

  /**
   * Generate image styles.
   */
  protected function generateImageStyles($config) {
    $id = $this->getDataName($config['field-name']);
    if (!ImageStyle::load($id)) {
      $style = ImageStyle::create([
        'name' => $id,
        'label' => $config['field-name'],
      ]);

      $style->addImageEffect([
        'id' => 'focal_point_scale_and_crop',
        'data' => [
          'width' => $config['max-image-width'],
          'height' => $config['max-image-height'],
        ],
      ]);

      $style->save();
    }
  }

}
