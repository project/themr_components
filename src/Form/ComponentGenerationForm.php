<?php

/**
 * @file
 * Contains \Drupal\themr_components\Form\ComponentGenerator.
 */

namespace Drupal\themr_components\Form;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\ContentEntityType;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\themr_components\ComponentGenerator;
use Drupal\themr_components\PluginManager\ThemrFieldManager;
use OpenAI\Client;
use OpenAI\Exceptions\ErrorException;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ComponentGenerationForm extends FormBase implements ContainerInjectionInterface {

  /**
   * Entity type manager.
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The entity field manager.
   */
  protected EntityFieldManagerInterface $entityFieldManager;

  /**
   * The entity type bundle info.
   */
  protected EntityTypeBundleInfoInterface $entityTypeBundleInfo;

  /**
   * The open ai client.
   */
  protected Client $openAi;

  /**
   * The component generator.
   */
  protected ComponentGenerator $componentGenerator;

  /**
   * The Themr Field manager.
   */
  protected ThemrFieldManager $themrFieldManager;

  /**
   * Constructor.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    EntityFieldManagerInterface $entityFieldManagerInterface,
    EntityTypeBundleInfoInterface $entityTypeBundleInfoInterface,
    Client $openAi,
    ComponentGenerator $componentGenerator,
    ThemrFieldManager $themrFieldManager,
  ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->entityFieldManager = $entityFieldManagerInterface;
    $this->entityTypeBundleInfo = $entityTypeBundleInfoInterface;
    $this->openAi = $openAi;
    $this->componentGenerator = $componentGenerator;
    $this->themrFieldManager = $themrFieldManager;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('openai.client'),
      $container->get('themr_components.generator'),
      $container->get('plugin.manager.themr_field_manager'),
    );
  }
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'themr_component_generator';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#attached']['library'][] = 'themr_components/excalidraw';
    $form['#attached']['library'][] = 'themr_components/excalidraw-loader';

    $form['component_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Component Name'),
      '#description' => $this->t('The name of the component you want to create.'),
      '#required' => TRUE,
    ];

    $form['generation_type'] = [
      '#title' => $this->t('Generation Type'),
      '#type' => 'select',
      '#options' => [
        '' => $this->t('Choose how to generate'),
        'library' => $this->t('Themr Component Library'),
        'private_library' => $this->t('Private Component Library'),
        'image' => $this->t('From Image'),
        'doodle_and_text' => $this->t('Doodle and Text'),
        'text' => $this->t('Describe with Text only'),
      ],
      '#default_value' => 'doodle_and_text',
      '#description' => $this->t('Choose the way you want to generate your components.'),
    ];

    $form['image'] = [
      '#type' => 'fieldset',
      '#states' => [
        'visible' => [
          ':input[name="generation_type"]' => ['value' => 'image'],
        ],
      ],
    ];

    $form['image']['image_upload'] = [
      '#type' => 'file',
      '#title' => $this->t('Component Image'),
      '#description' => $this->t('The image of the component you want to generate.'),
      '#states' => [
        'required' => [
          ':input[name="generation_type"]' => ['value' => 'image'],
        ],
      ],
    ];

    $form['image']['image_prompt'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Extra information'),
      '#description' => $this->t('If you want to give some extra information, please give it here.'),
    ];

    $form['doodle'] = [
      '#type' => 'fieldset',
      '#states' => [
        'visible' => [
          ':input[name="generation_type"]' => ['value' => 'doodle_and_text'],
        ],
      ],
    ];

    $form['doodle']['doodle'] = [
      '#markup' => '<h3>Doodle a component</h3><div id="doodle"></div>'
    ];

    $form['doodle']['doodle_base64'] = [
      '#type' => 'hidden',
      '#attributes' => [
        'id' => [
          'doodle-base-64',
        ],
      ],
    ];

    $form['doodle']['doodle_prompt'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Extra information'),
      '#description' => $this->t('If you want to give some extra information, please give it here.'),
    ];

    $form['list_view_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('This is a list view'),
      '#description' => $this->t('If this component should list and link to other entities, please check here and choose entity.'),
      '#states' => [
        'visible' => [
          ':input[name="generation_type"]' => ['!value' => ''],
        ],
      ],
    ];

    $entities = [];
    foreach ($this->getEntityTypesWithBundles() as $key => $data) {
      $entities[$key] = $data['name'];
    };

    $form['list_view_entity'] = [
      '#type' => 'select',
      '#title' => $this->t('List Content'),
      '#description' => $this->t('The content/entity types to list.'),
      '#options' => $entities,
      '#default_value' => 'node',
      '#states' => [
        'visible' => [
          ':input[name="list_view_enabled"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['generate_css'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Generate CSS'),
      '#default_value' => TRUE,
      '#description' => $this->t('This will try to generate CSS as well. NOTE: This will take longer time and incure more costs in OpenAI. CSS will not be optimized, hire a frontender for that.'),
      '#states' => [
        'visible' => [
          ':input[name="generation_type"]' => ['!value' => ''],
        ],
      ],
    ];

    $form['actions']['fake-submit'] = [
      '#type' => 'button',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $formState) {
    switch ($formState->getValue('generation_type')) {
      case 'image':
        $files = $this->getRequest()->files->get('files', []);
        $image = $files['image_upload'];
        $imageData = file_get_contents($image->getRealPath());
        $prompt = $this->imageComponentPrompt($formState->getValue('image_prompt'), 'image');
        $output = json_decode(str_replace(['```json', '```'], '', $this->testTestimonialsResponse()), TRUE);
        $response = $this->openAiChatRequest($prompt, [], [
          'data:' . $image->getMimeType() . ';base64,' . base64_encode($imageData),
        ]);
        print_r($response);
        if (!empty($response['choices'][0]['message']['content'])) {
          $output = json_decode(str_replace(['```json', '```'], '', $response['choices'][0]['message']['content']), TRUE);
        }
        $this->componentGenerator->generateComponent($formState->getValue('component_name'), $output);
        break;

      case 'doodle_and_text':
        $prompt = $this->imageComponentPrompt($formState->getValue('doodle_prompt'), 'doodle');
        $response = $this->openAiChatRequest($prompt, [], [
          $formState->getValue('doodle_base64'),
        ]);
        print_r($response);
        if (!empty($response['choices'][0]['message']['content'])) {
          $output = json_decode(str_replace(['```json', '```'], '', $response['choices'][0]['message']['content']), TRUE);
        }
        $this->componentGenerator->generateComponent($formState->getValue('component_name'), $output);
        break;
    }
  }

  public function testTestimonialsResponse() {
    return '```json
[
  {
    "type": "div",
    "container-name": "Testimonials Section",
    "attributes": {
      "class": "text-center py-5"
    },
    "contains": [
      {
        "type": "h2",
        "field-type": "string",
        "field-name": "Testimonials Header",
        "field-item-amount": 1,
        "attributes": {
          "class": "mb-4"
        },
        "css": {
          "color": "#4e4e4e"
        }
      },
      {
        "type": "div",
        "container-name": "Testimonials Cards Container",
        "attributes": {
          "class": "row row-cols-1 row-cols-md-3 g-4 justify-content-center"
        },
        "contains": [
          {
            "type": "div",
            "field-type": "custom_field",
            "field-name": "Testimonial Card",
            "field-item-amount": 3,
            "attributes": {
              "class": "col"
            },
            "contains": [
              {
                "type": "div",
                "container-name": "Testimonial Card Body",
                "attributes": {
                  "class": "card h-100 border-0"
                },
                "css": {
                  "backgroundColor": "#ffffff",
                  "boxShadow": "0 4px 6px rgba(0, 0, 0, 0.1)"
                },
                "contains": [
                  {
                    "type": "div",
                    "container-name": "Testimonial Text",
                    "attributes": {
                      "class": "card-body"
                    },
                    "contains": [
                      {
                        "type": "p",
                        "field-type": "text_long",
                        "field-name": "Testimonial Content",
                        "field-item-amount": 1,
                        "attributes": {
                          "class": "card-text"
                        }
                      }
                    ]
                  },
                  {
                    "type": "div",
                    "container-name": "Testimonial Footer",
                    "attributes": {
                      "class": "card-footer bg-transparent border-0"
                    },
                    "contains": [
                      {
                        "type": "img",
                        "field-type": "image",
                        "field-name": "Testimonial Author Image",
                        "field-item-amount": 1,
                        "attributes": {
                          "class": "rounded-circle mb-3",
                          "style": "width: 64px; height: 64px;"
                        },
                        "max-image-width": 64,
                        "max-image-height": 64
                      },
                      {
                        "type": "h5",
                        "field-type": "string",
                        "field-name": "Testimonial Author Name",
                        "field-item-amount": 1,
                        "attributes": {
                          "class": "mb-1"
                        }
                      },
                      {
                        "type": "h6",
                        "field-type": "string",
                        "field-name": "Testimonial Author Position",
                        "field-item-amount": 1,
                        "attributes": {
                          "class": "text-muted"
                        }
                      }
                    ]
                  }
                ]
              }
            ]
          }
        ]
      }
    ]
  }
]
```';
  }

  /**
   * Get prompt.
   *
   * @param string $extraCommands
   *   Extra commands to add.
   *
   * @return string
   *   The prompt.
   */
  public function imageComponentPrompt($extraCommands = "", $type = "image") {
    $fieldTypes = "";
    foreach ($this->themrFieldManager->getDefinitions() as $definition) {
      $fieldTypes .= $definition['id'] . ' - ' . $definition['description'] . "\n";
    }
    $prompt = "You are an expert Bootstrap frontend developer.

You take a $type from a component on a web page and then build html and css based on Bootstrap 5 for a component holding the information, but answer in JSON.

- Make sure the component looks exactly like the $type.
- Pay close attention to background color, text color, font size, font family, padding, margin, border, etc. Match the colors and sizes exactly.
- Do not add comments in the code such as \"<!-- Add other navigation links as needed -->\" and \"<!-- ... other news items ... -->\" in place of writing the full code. WRITE THE FULL CODE.

- Make sure you output all the micro components inside the main component class name and the extra inline css in its own data.
- Where data is supposed to be filled in, add the field type. If the data is a complex type used multiple types, add it as custom_field and then include each field type inside that field.
- Do start after the container. Do not include the container.

In terms of libraries,

- You can use Google Fonts
- You can use Font Awesome for icons

Custom field is extra important that it is set on any complex grouping of fields that are repeated, like cards. Set the field custom_field for each of these collection of fields and the possible amount to set.

Try to figure out all the CSS overrides that are not in the normal Bootstrap library.

The container-name and field-name all have to be unique.

The field types you can use are based on Drupal field types and are the following:
----------------------------------------
$fieldTypes
----------------------------------------

Do not include any explanations, only provide a RFC8259 compliant JSON response following this format for each element within the component without deviation:
---------------------------------------------
{
  \"type\": \"The HTML tag\",
  \"attributes\": \"Attribute objects, by attribute name as key and attributes values as one string value\",
  \"css\": \"Css objects outside of Bootstrap 5, by css name as key and css value as value\",
  \"field-type\": \"If this is a final object without any children or if this is a custom field, set the field type based on the Drupal field available\",
  \"field-name\": \"Think of a good field name for the field, it should be human readable in English\",
  \"field-item-amount\": \"The amount of items that can be filled in here. If unlimited set -1\",
  \"container-name\": \"If this is not a final object, think of a good unique container name this container, it should be human readable in English\",
  \"max-image-width\": \"If it is an image, try to calculate the largest width possible in a full hd container, answer in a number of pixels\",
  \"max-image-height\": \"If it is an image, try to calculate the largest height possible in a full hd container, answer in a number of pixels\",
  \"contains\": \"An array of child elements under this element. Use the same JSON for them\",
}
---------------------------------------------

Example of a simple hero section:
---------------------------------------------
[
  {
    \"type\": \"div\",
    \"container-name\": \"Section Row\",
    \"attributes\": [
      \"class\": \"row flex-lg-row-reverse align-items-center g-5 py-5\",
    ],
    \"contains\": [
      {
        \"type\": \"div\",
        \"container-name\": \"Image Wrapper\",
        \"attributes\": {
          \"class\": \"col-10 col-sm-8 col-lg-6\",
        },
        \"contains\": [
          {
            \"type\": \"img\",
            \"field-type\": \"image\",
            \"field-name\": \"Image\",
            \"field-item-amount\": 1,
            \"max-image-width\": 700,
            \"max-image-height\": 500,
            \"attributes\": {
              \"class\": \"d-block mx-lg-auto img-fluid\",
            },
          }
        ]
      },
      {
        \"type\": \"div\",
        \"container-name\": \"Information Wrapper\",
        \"attributes\": [
          {\"class\": \"col-lg-6\"},
        ],
        \"css\": [
          {\"background-color\": \"#000000\"},
        ]
        \"contains\": [
          {
            \"type\": \"h1\",
            \"field-type\": \"string\",
            \"field-name\": \"Header\",
            \"field-item-amount\": 1,
            \"css\": [
              {\"color\": \"#44ee44\"},
            ],
            \"attributes\": {
              \"class\": \"display-5 fw-bold lh-1 mb-3\",
            },
          },
          {
            \"type\": \"p\",
            \"field-type\": \"text_long\",
            \"field-name\": \"Description\",
            \"field-item-amount\": 1,
            \"css\": [
              {\"color\": \"#ffffff\"},
            ],
            \"attributes\": {
              \"class\": \"lead\",
            },
          },
          {
            \"type\": \"div\",
            \"container-name\": \"Button Wrapper\",
            \"attributes\": {
              \"class\": \"d-grid gap-2 d-md-flex justify-content-md-start\",
            },
            \"contains\": [
              {
                \"type\": \"a\",
                \"field-type\": \"link\",
                \"field-name\": \"Primary Button\",
                \"field-item-amount\": 1,
                \"attributes\": {
                  \"class\": \"btn btn-primary btn-lg px-4 me-md-2\",
                },
              },
              {
                \"type\": \"a\",
                \"field-type\": \"link\",
                \"field-name\": \"Secondary Button\",
                \"field-item-amount\": 1,
                \"attributes\": {
                  \"class\": \"btn btn-outline-secondary btn-lg px-4\",
                },
              },
            ]
          }
        ]
      }
    ]
  }
]
-----------------------------------------------

Example of a complex testimonials section:
-----------------------------------------------
[
  {
    \"type\": \"div\",
    \"container-name\": \"Initial Row\",
    \"attributes\": {
      \"class\": \"row d-flex justify-content-center\",
    },
    \"contains\": [
      {
        \"type\": \"div\",
        \"container-name\": \"Testiominal Introduction\",
        \"attributes\": {
          \"class\": \"col-md-10 col-xl-8 text-center\",
        },
        \"contains\": [
          {
            \"type\": \"h3\",
            \"field-type\": \"string\",
            \"field-name\": \"Header\",
            \"field-item-amount\": 1,
            \"attributes\": {
              \"class\": \"display-5 fw-bold lh-1 mb-3\",
            },
          },
          {
            \"type\": \"p\",
            \"field-type\": \"text_long\",
            \"field-name\": \"Description\",
            \"field-item-amount\": 1,
            \"attributes\": {
              \"class\": \"mb-4 pb-2 mb-md-5 pb-md-0\",
            },
          },
        ]
      },
      {
        \"type\": \"div\",
        \"container-name\": \"Testimonials\",
        \"attributes\": {
          \"class\": \"row text-center\",
        },
        \"contains\": [
          {
            \"type\": \"div\",
            \"field-type\": \"custom_field\",
            \"field-name\": \"Header\",
            \"field-item-amount\": 3,
            \"css\": [
              {\"border\": \"1px solid #000\"},
            ],
            \"attributes\": {
              \"class\": \"col-md-4 mb-5 mb-md-0\",
            },
            \"contains\": [
              {
                \"type\": \"div\",
                \"container-name\": \"Image Wrapper\",
                \"attributes\": {
                  \"class\": \"d-flex justify-content-center mb-4\",
                },
                \"contains\": [
                  {
                    \"type\": \"img\",
                    \"field-type\": \"image\",
                    \"field-name\": \"Testimonial Image\",
                    \"field-item-amount\": 1,
                    \"max-image-width\": 90,
                    \"max-image-height\": 90,
                    \"attributes\": {
                      \"class\": \"rounded-circle\",
                    },
                  }
                ]
              },
              {
                \"type\": \"h5\",
                \"field-type\": \"string\",
                \"field-name\": \"Name of Testimonial Giver\",
                \"field-item-amount\": 1,
                \"attributes\": {
                  \"class\": \"mb-3\",
                },
              },
              {
                \"type\": \"h6\",
                \"field-type\": \"string\",
                \"field-name\": \"Position of Testimonial Giver\",
                \"field-item-amount\": 1,
                \"attributes\": {
                  \"class\": \"text-primary mb-3\",
                },
              },
              {
                \"type\": \"em\",
                \"field-type\": \"text_long\",
                \"field-name\": \"Testimonial Text\",
                \"field-item-amount\": 1,
                \"attributes\": {
                  \"class\": \"px-xl-3 pe-2\",
                },
              },
            ]
          }
        ]
      }
    ]
  }
]
-----------------------------------------------

    ";

    if ($extraCommands) {
      $prompt .= "\n\nAlso follow the following commands: $extraCommands\n";
    }

    return $prompt;
  }

  /**
   * Get all entity types with bundle.
   *
   * @return array
   *   The entities and bundles.
   */
  public function getEntityTypesWithBundles() {
    $typesWithBundles = [];
    foreach($this->entityTypeManager->getDefinitions() as $entityName => $entityDefinition) {
      if ($entityDefinition instanceof ContentEntityType) {
        $typesWithBundles[$entityName] = [
          "machine_name" => $entityName,
          "name" => $entityDefinition->getLabel(),
          "bundles" => []
        ];
      }
    }
    foreach ($typesWithBundles as $entityTypekey => &$entityType) {
      foreach ($this->entityTypeBundleInfo->getBundleInfo($entityTypekey) as $bundle => $bundleInfo) {
        $entityType['bundles'][] = $bundle;
      }
    }
    return $typesWithBundles;
  }

  /**
   * Generate the OpenAI Chat request.
   *
   * @param string $prompt
   *   The prompt.
   * @param array $interpolatorConfig
   *   The interpolator config.
   * @param array $images
   *   Images to pass on to OpenAI Vision.
   *
   * @return string
   *   The response.
   */
  protected function openAiChatRequest($prompt, $config, $images = []) {
    $messages[] = [
      'role' => 'system',
      'content' => trim($prompt),
    ];
    $content[] = [
      "type" => "text",
      "text" => 'Generate the code for the component that looks exactly like this.'
    ];
    if (count($images)) {
      foreach ($images as $image) {
        $content[] = [
          "type" => "image_url",
          "image_url" => [
            "url" => $image,
          ],
        ];
      }
    }
    $messages[] = [
      'role' => 'user',
      'content' => $content,
    ];
    try {
      $response = $this->openAi->chat()->create([
        'model' => 'gpt-4-vision-preview',
        'messages' => $messages,
        'max_tokens' => 4000,
      ]);
    }
    catch (ErrorException $e) {
      return $e->getMessage();
    }

    $result = $response->toArray();
    return $result;
  }
}
